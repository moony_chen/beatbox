package com.bignerdranch.android.beatbox;

import android.content.Context;
import android.databinding.BaseObservable;

public class BeatBoxViewModel extends BaseObservable {

    private BeatBox mBeatBox;
    private Context mContext;

    private int speed = 100;

    public BeatBoxViewModel(BeatBox beatBox, Context context) {
        mBeatBox = beatBox;
        mContext = context;
    }

    public int getSpeed() {
        return speed;
    }

    public void setSpeed(int speed) {
        this.speed = speed;
    }

    public float getRate() {
        return speed / 100.0f;
    }

    public String getRateText() {
        return mContext.getString(R.string.speed_paly_text, getRate());
    }

    public void onProgressChanged(int b) {
        setSpeed(b);
        notifyChange();
    }
}
